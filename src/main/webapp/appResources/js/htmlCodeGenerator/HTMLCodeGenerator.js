var app = angular.module( 'HTMLCodeGenerator', [] );

app.service( 'PartialTagStringService', function (){

    var isKeyValuePairValid = function( node, key ){
        return typeof node[ key ] !== 'undefined' && node[key].length > 0;
    };

    this.getPartialTagString = function ( node ) {

        var keys = ['id', 'name', 'type', 'classes'];

        var string = '';

        angular.forEach( keys, function( key){

            if( isKeyValuePairValid( node, key )) {
                string = string + ( key === 'classes' ? 'class' : key ) + '="' + node[key] + '" ';
            }
        });

        return string;
    };
});

app.service( 'InputTagService', function ( PartialTagStringService ) {
    this.getInputTag = function ( node ) {
        return '<input ' + PartialTagStringService.getPartialTagString( node ) + '/>' + "\n";
    };
});

app.service( 'TagDescription', function () {

    var isKeyValuePairValid = function( node, key ){
        return typeof node[ key ] != 'undefined' && node[key].length > 0;
    };

    var capitalizeFirstLetter = function(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    };

    this.getTagInfo = function ( node ) {
        var keys = ['tagType', 'id', 'name', 'type', 'classes', 'value'];

        var description = '';

        angular.forEach( keys, function( key ) {
            if( key === 'tagType' ) {
                description = description + capitalizeFirstLetter( node.tagType ) + ", ";
            } else {
                description = description + ( isKeyValuePairValid( node, key ) ? ( '' + key + ' : ' + node[key] + ", " ) : '' );
            }
        });

        return description.trim().replace(new RegExp(',' + '$'), '');
    };
});

app.service( 'LabelTagService', function( PartialTagStringService ) {
    this.getTag = function( node ) {
        return '<label ' + PartialTagStringService.getPartialTagString( node ) + '>' + node.value + "</label>" + "\n";
    }
});

app.service( 'HTMLCodeGeneratorService', function( InputTagService, LabelTagService, PartialTagStringService, $rootScope){
    var self = this;

    this.currentIndentLevel = 0;
    this.indentSpaceLength = 5;
    this.indentString = '';

    for( var i=0; i< this.indentSpaceLength; i++ ) {
        this.indentString = this.indentString + ' ';
    }

    this.getStartingSpaces = function (indentLevel) {
        var toReturnIndentedString = '';
        for( var i = 0 ; i<indentLevel; i++  ) {
            toReturnIndentedString = toReturnIndentedString + self.indentString;
        }

        return toReturnIndentedString;
    };

    this.getHTMLCode = function ( node ) {
        self.currentIndentLevel = 0;

        return self.generateCodeForNode( node, self.currentIndentLevel );
    };

    this.generateCodeForNode = function ( node, indentLevel ) {
        if( node.tagType === 'input' ) {
            return self.getStartingSpaces(indentLevel) + InputTagService.getInputTag( node );
        } else if( node.tagType === 'label' ) {
            return self.getStartingSpaces( indentLevel ) + LabelTagService.getTag( node );
        } else if( node.tagType === 'form' ) {

            var childString = self.getStartingSpaces(indentLevel) + '<form ' + PartialTagStringService.getPartialTagString( node ) + '>' + "\n";

            angular.forEach( node.children, function( childNode ) {
                childString = childString + self.generateCodeForNode( childNode, indentLevel + 1 );
            });

            childString = childString + self.getStartingSpaces(indentLevel) +  '</form>' + "\n";

            return childString;

        } else if( node.tagType === 'div' ) {

            var childString = self.getStartingSpaces( indentLevel ) + '<div ' + PartialTagStringService.getPartialTagString( node ) + '>' + "\n"

            angular.forEach( node.children, function( childNode ) {
                childString = childString + self.generateCodeForNode( childNode, indentLevel + 1 );
            });

            childString = childString + self.getStartingSpaces(indentLevel) + '</div>' + "\n";

            return childString;

        } else if( node.tagType === 'div' && node.children.length == 0 ) {
            return self.getStartingSpaces( indentLevel ) + '<div ' + PartialTagStringService.getPartialTagString( node ) + '></div>' + "\n";
        }
    };
});