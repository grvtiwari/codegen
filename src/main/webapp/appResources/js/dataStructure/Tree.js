var Node = function( parent, node ) {
	this.Parent = parent;
	
	this.id = node.id;
	this.name = node.name;
	
	//div or input
	this.tagType = node.tagType;
	this.children = [];
	
	//input type='text'
	this.type = node.type;
	
	this.classes = node.classes;
    this.value = node.value;
};

Node.prototype.addChild = function( child ) {
	child.Parent = this;	
	this.children.push( child );
};

Node.prototype.getChildren = function () {
	return this.children;
};