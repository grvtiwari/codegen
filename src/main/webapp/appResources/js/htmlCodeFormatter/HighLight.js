var app = angular.module('CodeHightLigher', []);

app.directive( 'highlight', function( $interpolate, $window ) {
    return {
        restrict: 'EA',
        scope: {
            htmlCode: '='
        },
        compile: function (element, tAttrs) {
            var interplolateFunction = $interpolate(element.html(), true);
            element.html('');

            return function (scope, elem, attrs) {
                scope.$watch(function(){return scope.htmlCode;}, function (value) {
                    console.log( value );
                    elem.html(hljs.highlight('html', value).value);
                });
            }
        }
    };
});