var app = angular.module( 'codeGenerator', ['HTMLCodeGenerator', 'AnchorScroll', 'ui.bootstrap', 'CodeHightLigher'] );

app.controller( 'DefaultController', function($scope, HTMLCodeGeneratorService, TagDescription, $rootScope, SmoothScroll, $modal ) {

    $scope.addTagPaneActive = false;

    $scope.parentSelectedToAdd = null;
    $scope.newTag = {};

    $scope.tags = ['div', 'input', 'label'];
	
	$scope.moveToAnchor = function( toAnchor ) {
		SmoothScroll.scrollTo( toAnchor );
	};

    $scope.updateHTMLCode = function () {
        $scope.htmlCode = HTMLCodeGeneratorService.getHTMLCode( $scope.rootTag );
        document.getElementById( 'preview').innerHTML = $scope.htmlCode;
        /*document.getElementById( 'htmlText').innerText = $scope.htmlCode;*/
    };

    $scope.htmlCode = '';
    var root;
	
	$scope.rootTag = root = new Node( null, { id : 'root', name : 'root', tagType : 'div', classes : '' } );
    $scope.updateHTMLCode();

    $scope.isFormAdded = function () {
        var formFound = false;

        angular.forEach( root.children, function( child ) {
            if( child.tagType === 'form' ){
                formFound = true;
            }
        });

        return formFound;
    };

    $scope.isAddTagPaneActive = function () {
      return $scope.addTagPaneActive;
    };

    $scope.makeAddTagPaneActive = function ( parentTag ) {
        $scope.addTagPaneActive = true;
        $scope.parentSelectedToAdd = parentTag;
        $scope.newTag = {};
    };

    $scope.addTag = function() {
        var newTag = new Node( $scope.parentSelectedToAdd, {id : $scope.newTag.id, name : $scope.newTag.name, type : $scope.newTag.type, classes : $scope.newTag.classes, tagType : $scope.newTag.tagType, value : $scope.newTag.value });
        console.log( $scope.newTag );
        console.log( newTag );
        $scope.parentSelectedToAdd.addChild( newTag );

        $scope.addTagPaneActive = false;
        $scope.updateHTMLCode();
    };

    $scope.addForm = function (){
        var modalInstance = $modal.open({
            templateUrl : 'addForm.html',
            controller : 'AddFormController',
            size : 'sm'
        });

        modalInstance.result.then( function( result ){
            console.log( result );
            if( typeof result.name !== 'undefined' && result.name.length > 0 ){
                var formNode = new Node( null, { name : result.name, tagType : 'form', classes : result.classes });

                $scope.addTagTo( $scope.rootTag, formNode );
                $scope.formNode = formNode;
            }
        });
    };

    $scope.getHtmlCode = function () { return '' + $scope.htmlCode };

    $scope.addTagTo = function( parent, childNode ) {
        parent.addChild( childNode );

        $scope.updateHTMLCode();
    };

    $scope.getForms = function() {
        return $scope.rootTag.getChildren();
    };

    $scope.getChildDivs = function ( form ) {
        return form.getChildren();
    };

    $scope.tagTypeChange = function () {
        console.log( $scope.newTag.tagType );
    };

    $scope.isLabelSelected = function () {
        return $scope.newTag.tagType === 'label';
    };

    $scope.getTagInfo = function( node ) {
        return TagDescription.getTagInfo( node );
    };
});

app.controller( 'AddFormController', function($scope, $modalInstance ){
    $scope.form = {};

    $scope.createForm = function(){$modalInstance.close( $scope.form )};
    $scope.close = function(){$modalInstance.dismiss( 'close' )};
});